// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

use std::fmt;

#[derive(Debug)]
pub enum NostrSdkError {
    Generic { err: String },
}

impl fmt::Display for NostrSdkError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Generic { err } => write!(f, "{}", err),
        }
    }
}

impl From<nostr_sdk::Error> for NostrSdkError {
    fn from(e: nostr_sdk::Error) -> NostrSdkError {
        Self::Generic { err: e.to_string() }
    }
}
