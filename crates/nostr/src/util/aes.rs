// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

use std::fmt;

use aes::cipher::block_padding::Pkcs7;
use aes::cipher::{BlockDecryptMut, BlockEncryptMut, KeyIvInit};
use aes::Aes256;
use bitcoin::secp256k1::rand;
use cbc::{Decryptor, Encryptor};

type Aes256CbcEnc = Encryptor<Aes256>;
type Aes256CbcDec = Decryptor<Aes256>;

#[derive(Debug, Eq, PartialEq)]
pub enum Error {
    InvalidContentFormat,
    Base64Decode,
    WrongBlockMode,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::InvalidContentFormat => write!(f, "Invalid content format"),
            Self::Base64Decode => write!(f, "Error while decoding from base64"),
            Self::WrongBlockMode => write!(
                f,
                "Wrong encryption block mode. The content must be encrypted using CBC mode!"
            ),
        }
    }
}

impl std::error::Error for Error {}

pub fn encrypt(key: [u8; 32], content: &[u8]) -> String {
    let iv: [u8; 16] = rand::random();
    let cipher: Aes256CbcEnc = Aes256CbcEnc::new(&key.into(), &iv.into());
    let result: Vec<u8> = cipher.encrypt_padded_vec_mut::<Pkcs7>(content);
    format!("{}?iv={}", base64::encode(result), base64::encode(iv))
}

pub fn decrypt<S>(key: [u8; 32], content: S) -> Result<Vec<u8>, Error>
where
    S: Into<String>,
{
    let content: String = content.into();
    let parsed_content: Vec<&str> = content.split("?iv=").collect();
    if parsed_content.len() != 2 {
        return Err(Error::InvalidContentFormat);
    }

    let content: Vec<u8> = base64::decode(parsed_content[0]).map_err(|_| Error::Base64Decode)?;
    let iv: Vec<u8> = base64::decode(parsed_content[1]).map_err(|_| Error::Base64Decode)?;

    let cipher = Aes256CbcDec::new(&key.into(), iv.as_slice().into());
    let result = cipher
        .decrypt_padded_vec_mut::<Pkcs7>(&content)
        .map_err(|_| Error::WrongBlockMode)?;

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encryption_decryption() -> Result<(), Error> {
        let key = [0u8; 32];

        let content = String::from("Saturn, bringer of old age");
        let encrypted_content = encrypt(key, content.as_bytes());

        assert_eq!(
            decrypt(key, &encrypted_content)?,
            content.as_bytes().to_vec()
        );

        assert_eq!(
            decrypt(key, "invalidcontentformat").unwrap_err(),
            Error::InvalidContentFormat
        );
        assert_eq!(
            decrypt(key, "badbase64?iv=encode").unwrap_err(),
            Error::Base64Decode
        );

        //Content encrypted with aes256 using GCM mode
        assert_eq!(
            decrypt(
                key,
                "nseh0cQPEFID5C0CxYdcPwp091NhRQ==?iv=8PHy8/T19vf4+fr7/P3+/w=="
            )
            .unwrap_err(),
            Error::WrongBlockMode
        );

        Ok(())
    }
}
