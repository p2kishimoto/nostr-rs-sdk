// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

use std::fmt;

use crate::key::KeyError;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub enum Error {
    /// I/O error
    IO(String),
    /// AES error
    #[cfg(feature = "nip04")]
    Aes(crate::util::aes::Error),
    /// Key error
    Key(KeyError),
    /// ECDSA error
    Secp256k1(bitcoin::secp256k1::Error),
    /// BIP32 error
    #[cfg(feature = "nip06")]
    BIP32(bitcoin::util::bip32::Error),
    /// BIP39 error
    #[cfg(feature = "nip06")]
    BIP39(bip39::Error),
    /// Bech32 error
    Bech32(bitcoin::bech32::Error),
    /// Error serializing or deserializing JSON data
    Json(serde_json::Error),
    /// Hex decoding error
    Hex(bitcoin::hashes::hex::Error),
    /// Reqwest error
    Reqwest(reqwest::Error),
    /// Url parse error
    Url(url::ParseError),
    /// Generic error
    Generic(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IO(err) => write!(f, "I/O error: {}", err),
            #[cfg(feature = "nip04")]
            Self::Aes(err) => write!(f, "AES error: {}", err),
            Self::Key(err) => write!(f, "Key error: {}", err),
            Self::Secp256k1(err) => write!(f, "ECDSA error: {}", err),
            #[cfg(feature = "nip06")]
            Self::BIP32(err) => write!(f, "BIP32 error: {}", err),
            #[cfg(feature = "nip06")]
            Self::BIP39(err) => write!(f, "BIP39 error: {}", err),
            Self::Bech32(err) => write!(f, "Bech32 error: {}", err),
            Self::Json(err) => write!(f, "JSON error: {}", err),
            Self::Hex(err) => write!(f, "Hex decoding error: {}", err),
            Self::Reqwest(err) => write!(f, "Reqwest error: {}", err),
            Self::Url(err) => write!(f, "Impossible to parse URL: {}", err),
            Self::Generic(err) => write!(f, "{}", err),
        }
    }
}

impl std::error::Error for Error {}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::IO(err.to_string())
    }
}

#[cfg(feature = "nip04")]
impl From<crate::util::aes::Error> for Error {
    fn from(err: crate::util::aes::Error) -> Self {
        Self::Aes(err)
    }
}

impl From<KeyError> for Error {
    fn from(err: KeyError) -> Self {
        Self::Key(err)
    }
}

impl From<bitcoin::secp256k1::Error> for Error {
    fn from(err: bitcoin::secp256k1::Error) -> Self {
        Self::Secp256k1(err)
    }
}

#[cfg(feature = "nip06")]
impl From<bitcoin::util::bip32::Error> for Error {
    fn from(err: bitcoin::util::bip32::Error) -> Self {
        Self::BIP32(err)
    }
}

#[cfg(feature = "nip06")]
impl From<bip39::Error> for Error {
    fn from(err: bip39::Error) -> Self {
        Self::BIP39(err)
    }
}

impl From<bitcoin::bech32::Error> for Error {
    fn from(err: bitcoin::bech32::Error) -> Self {
        Self::Bech32(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Self {
        Self::Json(err)
    }
}

impl From<bitcoin::hashes::hex::Error> for Error {
    fn from(err: bitcoin::hashes::hex::Error) -> Self {
        Self::Hex(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        Self::Reqwest(err)
    }
}

impl From<url::ParseError> for Error {
    fn from(err: url::ParseError) -> Self {
        Self::Url(err)
    }
}
