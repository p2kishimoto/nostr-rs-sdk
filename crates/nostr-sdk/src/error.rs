// Copyright (c) 2022 Yuki Kishimoto
// Distributed under the MIT software license

use std::fmt;

use crate::relay::pool::RelayPoolError;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub enum Error {
    /// I/O error
    IO(String),
    /// Nostr error
    Nostr(nostr::Error),
    /// Relay Pool error
    RelayPool(RelayPoolError),
    /// Url parse error
    Url(nostr::url::ParseError),
    /// Ws error
    Ws(tokio_tungstenite::tungstenite::Error),
    /// ECDSA error
    Secp256k1(nostr::secp256k1::Error),
    /// Hex decoding error
    Hex(nostr::hashes::hex::Error),
    /// Generic error
    Generic(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IO(err) => write!(f, "I/O error: {}", err),
            Self::Nostr(err) => write!(f, "Nostr error: {}", err),
            Self::RelayPool(err) => write!(f, "Relay pool error: {}", err),
            Self::Url(err) => write!(f, "Impossible to parse URL: {}", err),
            Self::Ws(err) => write!(f, "Ws error: {}", err),
            Self::Secp256k1(err) => write!(f, "ECDSA error: {}", err),
            Self::Hex(err) => write!(f, "Hex decoding error: {}", err),
            Self::Generic(err) => write!(f, "{}", err),
        }
    }
}

impl std::error::Error for Error {}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::IO(err.to_string())
    }
}

impl From<nostr::Error> for Error {
    fn from(err: nostr::Error) -> Self {
        Self::Nostr(err)
    }
}

impl From<nostr::key::KeyError> for Error {
    fn from(err: nostr::key::KeyError) -> Self {
        Self::Nostr(nostr::Error::Key(err))
    }
}

impl From<RelayPoolError> for Error {
    fn from(err: RelayPoolError) -> Self {
        Self::RelayPool(err)
    }
}

impl From<nostr::url::ParseError> for Error {
    fn from(err: nostr::url::ParseError) -> Self {
        Self::Url(err)
    }
}

impl From<tokio_tungstenite::tungstenite::Error> for Error {
    fn from(err: tokio_tungstenite::tungstenite::Error) -> Self {
        Self::Ws(err)
    }
}

impl From<nostr::secp256k1::Error> for Error {
    fn from(err: nostr::secp256k1::Error) -> Self {
        Self::Secp256k1(err)
    }
}

impl From<nostr::hashes::hex::Error> for Error {
    fn from(err: nostr::hashes::hex::Error) -> Self {
        Self::Hex(err)
    }
}
